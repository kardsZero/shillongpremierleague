import axios from 'axios';
import React, { useEffect, useState } from 'react';
import AdminPage from './AdminPage';

const token = localStorage.getItem('token');
const axiosInstance = axios.create({
    headers: {
        Authorization: `Bearer ${token}`,
    },
});

function AdminDelete() {
    const [isDelete, setIsDelete] = useState([]);

    const handleDelete = (toDeleteId) => {
        const idToDelete = { id: toDeleteId };
        const idToDeleteJSON = JSON.stringify(idToDelete);

        // Show a confirmation dialog before proceeding with the delete
        const shouldDelete = window.confirm('Are you sure you want to delete this item?');

        if (shouldDelete) {
            axiosInstance
                .delete(`http://192.168.1.48/ShillongPremiereLeague/delete/D_AllTeam.php`, {
                    data: idToDeleteJSON,
                    headers: {
                        'Content-Type': 'application/json',  
                    },
                })
                .then(() => {
                    setIsDelete((prevData) => prevData.filter((item) => item.id !== toDeleteId));
                })
                .catch((err) => console.log(err));
        }
    };

    useEffect(() => {
        axiosInstance
            .get('http://192.168.1.48/ShillongPremiereLeague/read/R_DeleteAllTeam.php')
            .then((res) => {
                setIsDelete(res.data.data);
            })
            .catch((err) => {
                console.error(err);
            });
    },[]);

    return (
        <div className='mt-14'>
            <AdminPage />
            <div className="container mx-auto px-4">
                <h2 className="text-2xl font-bold mb-4">Delete Teams</h2>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                    {isDelete.map((item) => (
                        <div key={item.id} className="bg-white p-4 rounded-lg shadow-md">
                            <h3 className="text-lg font-semibold mb-2">{item.teamName}</h3>
                            <button
                                onClick={() => handleDelete(item.id)}
                                className="px-3 py-1 bg-red-500 text-white rounded hover:bg-red-600 transition duration-300"
                            >
                                Delete
                            </button>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default AdminDelete;
