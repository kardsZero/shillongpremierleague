import React from 'react'
import { useNavigate } from 'react-router-dom'
function AdminLogout() {
    const navigate = useNavigate()
    const handleLogout =()=>{
        localStorage.removeItem('token')
        navigate('/adminLogin')
    }
  return (
    <div className='mt-14'>
      <button onClick={handleLogout}>Log Out</button>
    </div>
  )
}

export default AdminLogout
