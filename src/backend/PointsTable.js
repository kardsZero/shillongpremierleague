import axios from "axios";
import React, { useState, useEffect } from "react";
import AdminPage from "./AdminPage";

function PointsTable() {
  const [points, setPoints] = useState([]);
  const [data, setdata] = useState({
    teamName: "",
    matchPlay: "",
    win: "",
    draw: "",
    lose: "",
    goalFor: "",
    goalAgainst: "",
    points: "",
  });

  const handleSubmit = (e) => {
    const token = localStorage.getItem("token");
    e.preventDefault();
    console.log(data);
    axios
      .post("http://192.168.1.48/ShillongPremiereLeague/create/C_Point.php", data, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        // setdata
        window.alert('Points Inserted')
        console.log(res);
      })
      .catch((err) => console.log(err));
      // window.alert('Failed to insert points')
  };

  useEffect(() => {
    axios
      .get(
        "http://192.168.1.48/ShillongPremiereLeague/read/R_TeamNotInPoint.php"
      )
      .then((res) => setPoints(res.data.data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      <AdminPage />
      <div className="mt-14 p-4 ">
        <div className="flex flex-col items-center">
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="teamSelect" className="mr-2">
              Select Club:
            </label>
            <br/>
            <select
            required
              id="teamSelect"
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, teamName: e.target.value })}
            >
              <option value="" disabled selected>
                --Select Team--
              </option>
              {points.map((item) => (
                <option key={item.teamName} value={item.teamName}>
                  {item.teamName}
                </option>
              ))}
            </select>
          </div>

          <div className="mb-4">
            <label htmlFor="mpInput" className="mr-2">
              MP:
            </label>
            <input
            required
              type="number"
              id="mpInput"
              // value={matchPlay}
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, matchPlay: e.target.value })}
            />
          </div>

          <div className="mb-4">
            <label htmlFor="wInput" className="mr-2">
              W:
            </label>
            <input
            required
              type="number"
              id="wInput"
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, win: e.target.value })}
            />
          </div>

          <div className="mb-4">
            <label htmlFor="dInput" className="mr-2">
              D:
            </label>
            <input
            required
              type="number"
              id="dInput"
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, draw: e.target.value })}
            />
          </div>

          <div className="mb-4">
            <label htmlFor="lInput" className="mr-2">
              L:
            </label>
            <input
            required
              type="number"
              id="lInput"
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, lose: e.target.value })}
            />
          </div>

          <div className="mb-4">
            <label htmlFor="gfInput" className="mr-2">
              GF:
            </label>
            <input
            required
              type="number"
              id="gfInput"
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, goalFor: e.target.value })}
            />
          </div>

          <div className="mb-4">
            <label htmlFor="gaInput" className="mr-2">
              GA:
            </label>
            <input
            required
              type="number"
              id="gaInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setdata({ ...data, goalAgainst: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="ptsInput" className="mr-2">
              Pts:
            </label>
            <input
            required
              type="number"
              id="ptsInput"
              className="border border-black rounded-md p-1"
              onChange={(e) => setdata({ ...data, points: e.target.value })}
            />
          </div>

          <input
            type="submit"
            value="Submit"
            className="border border-black rounded-md bg-blue-500 text-white p-2 cursor-pointer"
          />
        </form>
        </div>
      </div>
    </>
  );
}

export default PointsTable;
