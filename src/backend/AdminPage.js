import React, { useState } from "react";
import { NavLink, Outlet } from "react-router-dom";
import { useNavigate } from "react-router-dom";

function AdminPage() {
  const navigate = useNavigate();
  const handleLogout = () => {
    localStorage.removeItem("token");
    navigate("/adminLogin");
  };

  const [isSubMenuVisible, setSubMenuVisible] = useState(false); // State variable for submenu visibility

  const toggleSubMenu = () => {
    // Toggle submenu visibility when "Gallery Submenu" is clicked
    setSubMenuVisible(!isSubMenuVisible);
  };
  const navActive = (isActive) => {
    return {
      textDecoration: isActive ? "underline" : "none",
    };
  };

  return (
    <div>
      <aside className="bg-slate-500 w-1/6 float-left h-auto right-0 ">
        <ul className="py-4 space-y-2">
          <li>
            <NavLink
              to="/CreateAllTeam"
              className="text-white px-4 py-2 block hover:bg-slate-600"
              style={navActive(window.location.pathname === "/CreateAllTeam")}
            >
              Create All Team
            </NavLink>
          </li>

          <li>
            <NavLink
              to="/CreateFixture"
              className="text-white px-4 py-2 block hover:bg-slate-600"
              style={navActive(window.location.pathname === "/CreateFixture")}
            >
              Create Fixture
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/updateAllTeam"
              className="text-white px-4 py-2 block hover:bg-slate-600"
              style={navActive(window.location.pathname === "/updateAllTeam")}
            >
              Update Team
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/adminDelete"
              className="text-white px-4 py-2 block hover:bg-slate-600"
              style={navActive(window.location.pathname === "/adminDelete")}
            >
              Delete Team
            </NavLink>
          </li>

          <li>
            {/* Parent NavLink for the submenu with onClick to toggle visibility */}
            <NavLink
              to="#"
              className="text-white px-4 py-2 block hover:bg-slate-600"
              onClick={toggleSubMenu}
            >
              <div
                // className={`${
                //   isSubMenuVisible ? "bg-blue-500" : "" // Add background color to the submenu portion
                // }`}
              >
                Gallery Submenu {isSubMenuVisible ? <span className="text-red-500 text-lg font-extrabold">↑</span> : <span className="text-red-500 text-lg font-extrabold">↓</span>}
              </div>
            </NavLink>
            {/* Conditionally render the submenu based on visibility */}
            {isSubMenuVisible && (
              <ul>
                <li>
                  <NavLink
                    to="/adminGalleryDelete"
                    className="text-white bg-blue-500 px-4 py-2 block hover:bg-slate-600"
                    style={navActive(
                      window.location.pathname === "/adminGalleryDelete"
                    )}
                  >
                    Admin Gallery Delete
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/insertToGallery"
                    className="text-white bg-blue-500 px-4 py-2 block hover:bg-slate-600"
                    style={navActive(
                      window.location.pathname === "/insertToGallery"
                    )}
                  >
                    Insert To Gallery
                  </NavLink>
                </li>
              </ul>
            )}
          </li>
          <li>
            <NavLink
              to="/updatepointsTable"
              className="text-white px-4 py-2 block hover:bg-slate-600"
              style={navActive(
                window.location.pathname === "/updatepointsTable"
              )}
            >
              Update Point To Table
            </NavLink>
          </li>
          <li>
            <button
              className="text-white px-4 py-2 block hover:bg-slate-600"
              onClick={handleLogout}
            >
              Admin Logout
            </button>
          </li>
        </ul>
        <Outlet />
      </aside>
      <p className="mt-14 text-5xl font-semibold shadow-md my-8 mx-8">
        Welcome Admin
      </p>
    </div>
  );
}

export default AdminPage;
