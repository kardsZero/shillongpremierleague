import React, { useState, useEffect } from "react";
import axios from "axios";
import AdminPage from "./AdminPage";

function CreateFixture() {
  const [team, setTeam] = useState([]);
  const [venue, setVenue] = useState([]);
  const [formData, setFormData] = useState({
    teamA: "",
    score: "",
    teamB: "",
    date: "",
    time: "",
    venue: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const venueApi =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_Venue.php";
  const readApi =
    "http://192.168.1.48/ShillongPremiereLeague/Read/R_AllTeam.php";
  const token = localStorage.getItem("token");
  useEffect(() => {
    // Fetch team data when the component mounts
    axios
      .get(readApi)
      .then((res) => {
        setTeam(res.data.data);
      })
      .catch((err) => {
        console.error(err);
      });

    // Fetch venue data when the component mounts
    axios
      .get(venueApi)
      .then((res) => {
        const venuesArray = res.data.data.map(({ id, venueName }) => ({
          venue: venueName,
          id: id,
        }));
        console.log(venuesArray); // Log the mapped venuesArray
        setVenue(venuesArray);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        "http://192.168.1.48/ShillongPremiereLeague/create/C_GamePlay.php",
        formData,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        console.log(res.data);

        // Check if the response indicates a successful creation (you may need to adjust this condition)
        // if (res.status === 200 || res.status === 201) {
        //   window.alert("Game play created successfully!");
        // }else if (res.status === 422){
        //   window.alert(res.data.message)
        // }
        // else {
        //   window.alert("An unexpected error occurred."); // Handle other status codes
        // }

        if(res.status === 422){
          // debugger
          console.log("hi im an error",res.data.message);
          window.alert(res.data.message)
        }else if(res.status === 200 || res.status ===201){
          window.alert(res.data.message)
        }
      }
      
      )
      .catch((err) => {
        console.error('hii im error',err);
        window.alert("team Cannot be off the same Name")
      });
  };

  return (
    <div className="p-4">
      <AdminPage />
      <form onSubmit={handleSubmit} className="space-y-4">
        <select name="teamA" onChange={handleChange} value={formData.teamA} className="border border-black p-2 rounded" required>
          <option unselectable="--Select Team--">--Select Team--</option>
          {team.map((teams) => (
            <option key={teams.teamName} value={teams.teamName}>
              {teams.teamName}
            </option>
          ))}
        </select>
        <input
          type="text"
          name="score"
          placeholder="Score"
          value={formData.score}
          onChange={handleChange}
          className="border border-black p-2 rounded"
          required
        />
        <select name="teamB" onChange={handleChange} value={formData.teamB} className="border border-black p-2 rounded" required>
          <option unselectable="--Select Team--">--Select Team--</option>
          {team.map((teams) => (
            <option key={teams.teamName} value={teams.teamName}>
              {teams.teamName}
            </option>
          ))}
        </select>

        <input
          type="date"
          name="date"
          placeholder="Date"
          value={formData.date}
          onChange={handleChange}
          className="border border-gray-300 p-2 rounded"
          required
        />

        <input
          type="time"
          name="time"
          placeholder="Time"
          value={formData.time}
          onChange={handleChange}
          className="border border-gray-300 p-2 rounded"
          required
        />

        <select
          name="venue"
          onChange={handleChange}
          value={formData.venue}
          className="border border-black p-2 rounded"
          required
        >
          <option unselectable="--Select venue--">--Select venue--</option>
          {venue.map((venues) => (
            <option key={venues.id} value={venues.venueName}>
              {venues.venue}
            </option>
          ))}
        </select>

        <button
          type="submit"
          className="bg-blue-500 text-white py-2 px-4 rounded"
        >
          Insert
        </button>
      </form>
    </div>
  );
}

export default CreateFixture;
