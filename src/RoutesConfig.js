import React from 'react';
import { Route, Routes } from 'react-router-dom'; 
import Home from './components/Home';
import About from './components/About';
import Gallery from './components/Gallery';
import TableAndFixture from './components/TableAndFixture';
import CreateAllTeam from './backend/CreateAllTeam';
import CreateFixture from './backend/CreateFixture';
import InsertToGallery from './backend/InsertToGallery';
import AdminPage from './backend/AdminPage';
import AdminLogin from './backend/AdminLogin';
import TeamDetails from './components/TeamDetails';
import UpdateAllTeam from './backend/UpdateAllTeam';
import AdminRegistration from './backend/AdminRegistration';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import AdminDelete from './backend/AdminDelete';
import AdminGalleryDelete from './backend/AdminGalleryDelete';
import PointsTable from './backend/PointsTable';
import UpdatePointsTable from './backend/UpdatePointsTable';

function RoutesConfig() {
  return (
    <>
    <Navbar/>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/gallery" element={<Gallery />} />
      <Route path="/tableAndFixture" element={<TableAndFixture />} />
      <Route path="/createAllTeam" element={<CreateAllTeam />} />
      <Route path="/createFixture" element={<CreateFixture />} />
      <Route path="/adminPage" element={<AdminPage />} />
      <Route path="/teamDetails/:teamId" element={<TeamDetails />} />
      <Route path="/updateAllTeam" element={<UpdateAllTeam />} />
      <Route path="/updateAllTeam/:imageID" element={<UpdateAllTeam />} />
      <Route path="/adminLogin" element={<AdminLogin />} />
      <Route path='/adminRegistration' element={<AdminRegistration/>}/>
      <Route path='/adminDelete' element={<AdminDelete/>}/> 
      <Route path='/pointsTable' element={<PointsTable/>}/>
      <Route path='/updatePointsTable' element={<UpdatePointsTable/>}/>
      <Route path="/adminGalleryDelete" element={<AdminGalleryDelete />} />
      <Route path="/insertToGallery" element={<InsertToGallery />} />
    </Routes>
    <Footer/>
    </>
  );
}

export default RoutesConfig;
