import React, { useEffect, useState } from "react";
import axios from "axios";
import { Carousel } from "react-responsive-carousel";
import { useParams } from "react-router-dom";

function TeamDetails() {
  const carouselStyle = {
    maxWidth: "600px", // Adjust the maximum width as desired
    margin: "0 auto", // Center the carousel
  };

  const imageStyle = {
    maxWidth: "100%",
    height: "300px", // Adjust the height as desired
  };

  const { teamId } = useParams();
  const kitPort =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_AllTeam.php";
  const baseUrl = "http://192.168.1.48/ShillongPremiereLeague/Read/";
  const [teamData, setTeamData] = useState(null);

  useEffect(() => {
    if (teamId) {
      const apiUrl = `${kitPort}?teamId=${teamId}`;
      axios
        .get(apiUrl)
        .then((res) => {
          if (res.data.data && Array.isArray(res.data.data)) {
            const selectedTeam = res.data.data.find(
              (team) => team.teamId === parseInt(teamId)
            );
            if (selectedTeam) {
              setTeamData(selectedTeam);
            } else {
              console.error("Team not found for teamId:", teamId);
            }
          } else {
            console.error("API response does not contain team data:", res.data);
          }
        })
        .catch((err) => console.error(err));
    }
  }, [teamId]);

  if (!teamData) {
    return <div className="mx-auto mt-10 text-center">Loading...</div>;
  }

  return (
    <div className="mx-4">
      <h2 className="mt-14 text-2xl font-bold">{teamData.teamName}</h2>
      <div style={carouselStyle}>
        <Carousel showArrows={true} showThumbs={false} showStatus={true}>
          {teamData.teamPhoto.map((photo) => (
            <div className="relative" key={photo.imageId}>
              <img
                src={`${baseUrl}${photo.imageSrc}`}
                alt={teamData.teamName}
                style={imageStyle}
              />
            </div>
          ))}
        </Carousel>
      </div>

      <div className=" mt-4 shadow-md my-6 mx-40">
        <p className="text-gray-700 text-justify text-lg font-serif">{teamData.teamDescription}</p>
      </div>
      <section className="text-gray-600 body-font mt-6">
        <div className="lg:w-3/4 lg:mx-auto md:w-2/3 w-5/6 mb-10 md:mb-0 flex justify-center md:ml-36">
          <div className="lg:flex md:block sm:block ">
            <div>
              <img
                className="object-cover object-top rounded h-96 w-96"
                alt="coach"
                src={`${baseUrl}${teamData.coachPhoto}`}
              />
            </div>
            <div className="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16">
              <p className="mb-8 leading-relaxed text-justify">
                {teamData.coachDescription}
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default TeamDetails;
