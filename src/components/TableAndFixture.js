import React, { useEffect, useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import axios from "axios";
function TableAndFixture() {
  const [activeTab, setActiveTab] = useState(0);
  const [fixture, setFixture] = useState([]);
  const [points,setPoints] = useState([])
  const fixtureApi =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_Gameplay.php";
    const pointsApi ="http://192.168.1.48/ShillongPremiereLeague/read/R_Point.php"
  const baseUrl = "http://192.168.1.48/ShillongPremiereLeague/Read/";
  useEffect(() => {
    axios
      .get(fixtureApi)
      .then((res) => setFixture(res.data.data))
      .catch((err) => console.log(err));
  }, []);

  useEffect(()=>{
    axios.get(pointsApi)
    .then((res)=>setPoints(res.data.data))
    .catch((err)=>console.log(err))
  },[])
  return (
    <>
      <div className="mx-auto p-4 max-w-3xl mt-14">
        <Tabs selectedIndex={activeTab} onSelect={(item) => setActiveTab(item)}>
          <TabList className="flex justify-center space-x-4">
            <Tab
              className={`px-4 py-2  text-black font-bold text-lg rounded-lg ${
                activeTab === 0 ? " bg-black " : "cursor-pointer "
              }`}
            >
              Table
            </Tab>
            <Tab
              className={`px-4 py-2  text-black font-bold text-lg rounded-lg ${
                activeTab === 1 ? " bg-black" : "cursor-pointer"
              }`}
            >
              Fixture
            </Tab>
          </TabList>
          <TabPanel>
            <h2 className="text-2xl font-bold mb-4 text-center mt-10">
              Shillong Premier League Standings
            </h2>
            <table className="w-full border-collapse border border-gray-300">
              <thead className="bg-gray-200">
                <tr>
                  <th className="border border-gray-300 px-4 py-2">Club</th>
                  <th className="border border-gray-300 px-4 py-2">MP</th>
                  <th className="border border-gray-300 px-4 py-2">W</th>
                  <th className="border border-gray-300 px-4 py-2">D</th>
                  <th className="border border-gray-300 px-4 py-2">L</th>
                  <th className="border border-gray-300 px-4 py-2">GF</th>
                  <th className="border border-gray-300 px-4 py-2">GA</th>
                  <th className="border border-gray-300 px-4 py-2">Pts</th>
                </tr>
              </thead>
              <tbody>
                {points.map((p,i)=>(
                  <tr
                  key={p.id}
                  className={`${
                    i % 2 === 0 ? "bg-gray-100" : "bg-white"
                  }`}
                >
                  <td className="border border-gray-300 px-4 py-2">{p.teamName}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.matchPlay}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.win}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.draw}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.lose}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.goalFor}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.goalAgainst}</td>
                  <td className="border border-gray-300 px-4 py-2">{p.points}</td>
                </tr>
                 ))}
              </tbody>
            </table>
          </TabPanel>

          <TabPanel>
            <h2 className="text-2xl font-bold mb-4 text-center mt-10">
              Shillong Premier League Fixture
            </h2>
            <table className="w-full border-collapse border border-gray-300">
              <thead>
                <tr>
                  <th className="border border-gray-300 px-4 py-2">Team A</th>
                  <th className="border border-gray-300 px-4 py-2">Score</th>
                  <th className="border border-gray-300 px-4 py-2">Team B</th>
                  <th className="border border-gray-300 px-4 py-2">Time</th>
                  <th className="border border-gray-300 px-4 py-2">Date</th>
                  <th className="border border-gray-300 px-4 py-2">Venue</th>
                </tr>
              </thead>
              <tbody>
                {fixture.map((item, index) => (
                  <tr
                    key={index}
                    className={`${
                      index % 2 === 0 ? "bg-gray-100" : "bg-white"
                    }`}
                  >
                    <td className="border border-gray-300 px-4 py-2">
                      {item.teamAName}
                      <center><img src={`${baseUrl}${item.teamALogo}`}  className="h-10 w-10"/></center>
                    </td>
                    <td className="border border-gray-300 px-4 py-2">
                      {item.score}
                    </td>
                    <td className="border border-gray-300 px-4 py-2 ">
                      {item.teamBName}
                      <center><img src={`${baseUrl}${item.teamBLogo}`} className="h-10 w-10"/></center>
                    </td>
                    <td className="border border-gray-300 px-4 py-2">
                      {item.time}
                    </td>
                    <td className="border border-gray-300 px-4 py-2">
                      {item.date}
                    </td>
                    <td className="border border-gray-300 px-4 py-2">
                      {item.venue}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </TabPanel>
        </Tabs>
      </div>
    </>
  );
}

export default TableAndFixture;
