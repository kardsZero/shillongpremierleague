import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel';

function MyCarousel() {
  const imageStyle = {
    maxWidth: '100%', // Ensure the image doesn't exceed its container's width
    height: '300px', // Set a fixed height for all images (adjust as needed)
  };

  return (
    <div className="w-full max-w-xl mx-auto">
      <Carousel showArrows={true} showThumbs={false} showStatus={false}>
        <div className="relative">
          <img src="heroImage.jpg" alt="Legend 1" style={imageStyle} />
          <p className="legend absolute inset-x-0 bottom-0 bg-black text-white p-2 text-center">Legend 1</p>
        </div>
        <div className="relative">
          <img src="logo192.png" alt="Legend 2" style={imageStyle} />
          <p className="legend absolute inset-x-0 bottom-0 bg-black text-white p-2 text-center">Legend 2</p>
        </div>
        <div className="relative">
          <img src="logo512.png" alt="Legend 3" style={imageStyle} />
          <p className="legend absolute inset-x-0 bottom-0 bg-black text-white p-2 text-center">Legend 3</p>
        </div>
      </Carousel>
    </div>
  );
}

export default MyCarousel;
